#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>

#define LOCKFILE "/var/run/simple_daemon.pid"
#define LOCKMODE (S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH)

int lockfile(int fd) {
  struct flock fl;
  fl.l_type = F_WRLCK;
  fl.l_start = 0;
  fl.l_whence = SEEK_SET;
  fl.l_len = 0;
  return(fcntl(fd, F_SETLK, &fl));
}

int already_running() {
  int fd;
  char buf[16];
  fd = open(LOCKFILE, O_RDWR | O_CREAT, LOCKMODE);
  if (fd < 0) {
    syslog(LOG_ERR, "simple_daemon: can not open %s: %s", LOCKFILE, strerror(errno));
    exit(1);
  }
  if (lockfile(fd) < 0) {
    if (errno == EACCES || errno == EAGAIN) {
      close(fd);
      return(1);
    }
    syslog(LOG_ERR, "simple_daemon: can not lock on %s: %s", LOCKFILE, strerror(errno));
    exit(1);
  }
  ftruncate(fd, 0);
  sprintf(buf, "%ld", (long) getpid());
  write(fd, buf, strlen(buf) + 1);
  return(0);
}

int main(void) {

  syslog(LOG_DEBUG, "simple daemon: start");

  /* Our process ID and Session ID */
  pid_t pid, sid;

  /* Fork off the parent process */
  pid = fork();
  if (pid < 0) {
    syslog(LOG_ERR, "simple daemon: pid < 0");
    exit(EXIT_FAILURE);
  }

  /* If we got a good PID, then
     we can exit the parent process. */
  if (pid > 0) {
    syslog(LOG_DEBUG, "simple daemon: pid > 0");
    exit(EXIT_SUCCESS);
  }

  /* Change the file mode mask */
  umask(0);

  /* Open any logs here */        

  /* Create a new SID for the child process */
  sid = setsid();
  if (sid < 0) {
    syslog(LOG_ERR, "simple daemon: sid < 0");
    /* Log the failure */
    exit(EXIT_FAILURE);
  }

  /* Change the current working directory */
  if ((chdir("/")) < 0) {
    syslog(LOG_ERR, "simple daemon: chdir(..) < 0");
    /* Log the failure */
    exit(EXIT_FAILURE);
  }

  // close all fd.

  /* Close out the standard file descriptors */
  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);

  if (already_running()) {
    syslog(LOG_ERR, "simple_daemon: daemon already running");
    exit(1);
  }

  /* Daemon-specific initialization goes here */

  /* The Big Loop */
  while (1) {
    /* Do some task here ... */

    syslog(LOG_DEBUG, "simple daemon: loop");
    sleep(30); /* wait 30 seconds */
  }

  syslog(LOG_DEBUG, "simple daemon: end");

  exit(EXIT_SUCCESS);
}
